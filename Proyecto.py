#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import time
import os


print('\t Bienvenido a conway')

while True:
    try:
        tamano = int(input('Ingrese tamaño: '))
        tiempo = int(input("Ingrese Velocidad: "))
        pro = int(input('Ingrese probabilidad %:'))
        proba = round( (tamano ** 2) * pro /100)
        
        if(tamano>0 and tiempo>0 and proba>0 ):
            break
    except ValueError:
        print('Por favor use un número Natural')

matriz = []

def evaluador(matriz, x, y, tamano):

    cont = 0

    # cuenta en una matriz de 3x3, y es estricto el 
    # limite, por lo que se usa (x+1)+1
    for i in range(x-1,x+2):
        for j in range(y-1,y+2):
            if(i<tamano and j<tamano and i>=0 and j>=0):
                if(matriz[i][j]==True):
                    cont = cont + 1
                else:
                    cont = cont
            else:
                pass

    # verifica el estado de esa celula.
    if(matriz[x][y] == None):
        # nace si hay 3 y esta muerta
        if(cont == 3):
            return True
        else:
            return None
    elif(matriz[x][y]==True):
        # se contara la misma celula por ello es 
        # el limite + 1
        if(cont == 3 or cont==4):
            return True
        else:
            return None
    
def imprime(matriz, tamano):

    vivas = 0

    for i in range(tamano):
        for j in range(tamano):
            if(matriz[i][j]==True):
                print('X', end='')
                vivas = vivas + 1
            else:
                print('-', end='')
        print('')
    muertas = (tamano ** 2) - vivas
    print('Vivas:', vivas)
    print('Muertas:', muertas)

    if(vivas is 0):
        return False
    else:
        return True

for i in range(tamano):
    matriz.append([None]*tamano)

for x in range(tamano):
    for y in range(tamano):
        azar = random.randrange(tamano**2)
       
        if(azar <= proba):
            matriz[x][y] = True

print('\n\n')



imprime(matriz, tamano)
matriz_tempo = []
for i in range(tamano):
    matriz_tempo.append([None]*tamano)


while True:
    os.system('clear')
    for x in range(tamano):
        for y in range(tamano):
            matriz_tempo[x][y] = evaluador(matriz, x,
                                            y,tamano)

    matriz = matriz_tempo
    time.sleep(tiempo)
    print('\nMatriz\n')
    opc = imprime(matriz, tamano)


    if(opc == False):
        break